package com.electricalweb.controllers;

import com.electricalweb.entities.User;
import com.electricalweb.service.UserService;

import javax.persistence.NoResultException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "IndexController", urlPatterns = "/index")
public class IndexController extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "/index.jsp";
        forwardResponse(url, request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<String> violations = new ArrayList<>();
        request.setAttribute("login", request.getParameter("login"));
        authoriseUser(request, violations);
        if (!violations.isEmpty()) {
            request.setAttribute("violations", violations);
        }
        String url = determineUrl(violations);
        forwardResponse(url, request, response);
    }

    private void authoriseUser(HttpServletRequest request, List<String> violations){
        UserService service= new UserService();
//        Connection conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "sa");
//        Statement stmt = conn.createStatement();
//        String sql = "SELECT login, password, email FROM users WHERE login = '" + request.getParameter("login") + "'";
//        ResultSet rs = stmt.executeQuery(sql);
//        rs.next();
//        User user = new User(rs.getString("login"), rs.getString("password"), rs.getString("email"));
//        rs.close();
        User user;
        try {
            user = service.findBy("login",request.getParameter("login"));
        } catch(NoResultException e){
            violations.add("wrong login");
            return;
        }
//        if (stmt != null) stmt.close();
        if (!request.getParameter("password").equals(user.getPassword())) {
            violations.add("wrong password");
            return;
        }
        request.setAttribute("password", user.getPassword());
        request.setAttribute("email", user.getEmail());
    }

    private String determineUrl(List<String> violations) {
        if (!violations.isEmpty()) {
            return "/";
        } else {
            return "/WEB-INF/views/userinfo.jsp";
        }
    }

    private void forwardResponse(String url, HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    private static class RequestUser {
//
//        private final String login;
//        private final String password;
//        private final String email;
//
//        private RequestUser(String login, String password, String email) {
//            this.login = login;
//            this.password = password;
//            this.email = email;
//        }
//
//        public static RequestUser fromRequestParameters(HttpServletRequest request) {
//            return new RequestUser(
//                    request.getParameter("login"),
//                    request.getParameter("password"),
//                    request.getParameter("email"));
//        }
//
//        public void setAsRequestAttributes(HttpServletRequest request) {
//            request.setAttribute("login", login);
//            request.setAttribute("password", password);
//            request.setAttribute("email", email);
//        }
//    }
}
