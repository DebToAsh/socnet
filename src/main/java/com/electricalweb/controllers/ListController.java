package com.electricalweb.controllers;

import com.electricalweb.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ListController", urlPatterns = "/list")
public class ListController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response){
        List listResults = convertToList(request);
        request.setAttribute("listResults", listResults);
        String url = "/WEB-INF/views/list.jsp";
        forwardResponse(url, request, response);
    }

    private ArrayList convertToList(HttpServletRequest request) {
        UserService service= new UserService();
        return (ArrayList)service.getAll();
    }

    private void forwardResponse(String url, HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
