package com.electricalweb.controllers;

import com.electricalweb.entities.User;
import com.electricalweb.service.UserService;
import com.electricalweb.validators.EmailValidator;
import com.electricalweb.validators.LoginValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "CustomerController", urlPatterns = "/signUp")
public class CustomerController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestUser user = RequestUser.fromRequestParameters(request);
        user.setAsRequestAttributes(request);
        List<String> violations = user.validate();

        if (!violations.isEmpty()) {
            request.setAttribute("violations", violations);
        } else {
            addToDB(user);
        }

        String url = determineUrl(violations);
        forwardResponse(url, request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "/WEB-INF/views/signUp.jsp";
        forwardResponse(url, request, response);
    }

    private void addToDB(RequestUser user){
        UserService service= new UserService();
        User userE=new User(user.login,user.password,user.email);
        service.add(userE);
//        Connection conn = DriverManager.getConnection("jdbc:h2:~/soc", "sa", "sa");
//        Statement stmt = conn.createStatement();
//        String sql = "INSERT INTO users (login, password, email) VALUES ('" + user.login + "', '" + user.password + "', '" + user.email + "')";
//        stmt.executeUpdate(sql);
//        if (stmt != null) stmt.close();
    }

    private String determineUrl(List<String> violations) {
        if (!violations.isEmpty()) {
            return "/WEB-INF/views/signUp.jsp";
        } else {
            return "/WEB-INF/views/userinfo.jsp";
        }
    }

    private void forwardResponse(String url, HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class RequestUser {

        private final String login;
        private final String password;
        private final String email;

        private RequestUser(String login, String password, String email) {
            this.login = login;
            this.password = password;
            this.email = email;
        }

        public static RequestUser fromRequestParameters(HttpServletRequest request) {
            return new RequestUser(
                    request.getParameter("login"),
                    request.getParameter("password"),
                    request.getParameter("email"));
        }

        public void setAsRequestAttributes(HttpServletRequest request) {
            request.setAttribute("login", login);
            request.setAttribute("password", password);
            request.setAttribute("email", email);
        }

        public List<String> validate() {
            List<String> violations = new ArrayList<>();
            if (!LoginValidator.validate(login)) {
                violations.add("This login is already taken");
            }
            if (!EmailValidator.validateWriting(email)) {
                violations.add("Email must be a well-formed address");
            }
            if (!EmailValidator.validateExisting(email)) {
                 violations.add("This email is already taken");
            }
            return violations;
        }
    }
}
