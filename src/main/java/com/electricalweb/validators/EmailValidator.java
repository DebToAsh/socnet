package com.electricalweb.validators;

import com.electricalweb.entities.User;
import com.electricalweb.service.UserService;

import javax.persistence.NoResultException;
import java.sql.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", Pattern.CASE_INSENSITIVE);

    public static boolean validateWriting(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

    public static boolean validateExisting(String email) {
        UserService service= new UserService();
        try {
            service.findBy("email",email);
        } catch(NoResultException e){
            return true;
        }
//        Connection conn = DriverManager.getConnection("jdbc:h2:~/soc", "sa", "sa");
//        Statement stmt = conn.createStatement();
//        String sql = "SELECT login, password, email FROM users WHERE email = '" + email + "'";
//        User user = null;
//        try {
//            ResultSet rs = stmt.executeQuery(sql);
//            rs.next();
//            user = new User(rs.getString("login"), rs.getString("password"), rs.getString("email"));
//            rs.close();
//        } catch (SQLException e) {
//            return true;
//        }
//        if (stmt != null) stmt.close();
        return false;
    }
}