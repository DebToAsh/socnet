<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User Data</title>
    <link href="bootstrap.css" type="text/css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="row">
    <div class="col-md-4">
        <div class="row">
            <span class="col-md-12 ml-2"><h1>Welcome!</h1></span>
        </div>
        <div class="row">
            <span class="col-md-12 ml-2"><h2>You provided the following data:</h2></span>
        </div>
        <div class="row">
            <div class="col-md-12 ml-2"><strong>Login: </strong> ${login}</div>
        </div>
        <div class="row">
            <div class="col-md-12 ml-2"><strong>password: </strong> ${password}</div>
        </div>
        <div class="row">
            <div class="col-md-12 ml-2"><strong>Email: </strong>${email}</div>
        </div>
    </div>
    <div class="col-md-6">
        <img src="some/rooHappy.png" alt="rooHappy" class="img-thumbnail">
    </div>
</div>
</body>
</html>