<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: DebT
  Date: 15.08.2018
  Time: 16:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>User list</title>
    <link href="bootstrap.css" type="text/css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="col-md-12 ml-3"><h3>User list</h3></div>
<div class="row">
    <div class="col-md-6 ml-3">
        <table class="table">
            <thead><tr><th>#</th>
                <th>Id</th>
                <th>Login</th>
                <th>Password</th>
                <th>Email</th>
            </tr></thead><tbody>
        <jsp:useBean id="listResults" class="java.util.ArrayList" scope="request"/>
        <c:set var="count" value="0" scope="page" />
        <c:forEach items="${listResults}" var="user">
        <tr><th scope="row"><c:set var="count" value="${count + 1}" scope="page"/> ${count}</th>
            <td>${user.id}</td>
            <td>${user.login}</td>
            <td>${user.password}</td>
            <td>${user.email}</td>
        </tr>
        </c:forEach>
    </div>
</div>
</body>
</html>
