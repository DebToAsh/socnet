<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User Log in</title>
    <link href="bootstrap.css" type="text/css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="row">
    <div class="col-md-4">
        <div class="row">
            <span class="col-md-12 ml-3"><h1>User Login</h1></span>
        </div>
        <br>
        <c:if test="${violations != null}">
            <c:forEach items="${violations}" var="violation">
                <div class="row">
                    <div class="col-md-12"><p align="center" class="bg-danger">${violation}.</p></div>
                </div>
            </c:forEach>
        </c:if>
        <br>


        <form class="form-horizontal" action="${pageContext.request.contextPath}/index" method="post">
            <div class="row">

                <div class="form-group col-md-12">
                    <label for="inputLogin" class="col-sm-2 control-label"><h4>Login</h4></label>
                    <div class="col-sm-12">
                        <input type="login" class="form-control" name="login" id="inputLogin" placeholder="Login"
                               required="required" value="${login}">
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="form-group col-md-12">
                    <label for="inputPassword" class="col-sm-2 control-label"><h4>Password</h4></label>
                    <div class="col-sm-12">
                        <input type="password" class="form-control" name="password" id="inputPassword"
                               placeholder="Password" required="required" value="${password}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <a href="${pageContext.request.contextPath}/signUp" class="btn btn-primary ml-3">Sign Up</a>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-3" align="right">
                    <button type="submit" class="btn btn-primary mr-3">Log In</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-6">
        <img src="some/rooVV.jpg" alt="rooVV" class="img-thumbnail">
    </div>
</div>
</body>
</html>